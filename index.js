import React from 'react';
import ReactDOM from 'react-dom';

import './src/styles/styles.scss';

import Header from './src/components/main/Header/Header';
import MainContent from './src/components/main/MainContent/MainContent/MainContent';
import Footer from './src/components/main/Footer/Footer';

class App extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <MainContent />
                <Footer />
            </div>);
    };
}

ReactDOM.render(<App />, document.getElementById('root'));
