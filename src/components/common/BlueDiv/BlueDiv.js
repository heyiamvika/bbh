import React from 'react';

class BlueDiv extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const mq = window.matchMedia("(max-width: 760px)");
        let style;

        if (mq.matches) {
            style = {
                width: '309px',
                height: '34px',
                backgroundColor: '#4bc6e8'
            }
        } else {
            style = {
                width: this.props.width,
                height: '34px',
                backgroundColor: '#4bc6e8'
            }
        }

        return (
            <div style={style}></div>
        );
    }
};

export default BlueDiv;