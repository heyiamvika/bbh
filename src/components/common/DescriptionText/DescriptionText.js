import React from 'react';

import './c-description-text.scss';
import './mobile-description-text.scss';

class DescriptionText extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="c-description-text">
                <div className="preview">
                    <p className="subheading subheading-blue">闪耀在每个瞬间</p>
                    <p className="heading white-bold-heading">珍爱影片</p>
                </div>
                <div className="paragraph">
                    <p>在茫茫十三亿人中我遇见了你
                        这一辈子有多少个早晨，就有多少次机会对你好一点
                        让你有60个暖心的冬天，让你365天 每天睡个好觉
                        只因这不变的闪烁，让我们每天都活在爱中
                    </p>
                </div>
            </div >
        );
    }
};

export default DescriptionText;