import React from 'react';

import './c-diamond-nav.scss';
import './mobile-diamond-nav.scss';

import diamondBlue from './assets/diamond-blue.svg';
import diamondWhite from './assets/diamond-white.svg';

class DiamondNav extends React.Component {

    render() {
        return (
            <div className="c-diamond-nav">
                <ul className="diamonds">
                    <li><img src={diamondBlue} /></li>
                    <li><img src={diamondWhite} /></li>
                    <li><img src={diamondWhite} /></li>
                    <li><img src={diamondWhite} /></li>
                    <li><img src={diamondWhite} /></li>
                </ul>
            </div>
        );
    }
};

export default DiamondNav;