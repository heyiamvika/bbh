import React from 'react';

import './c-page-nav.scss';
import './mobile-page-nav.scss';

import arrowDown from '../../../assets/scroll-down.svg';

class PageNav extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <nav className="c-page-nav">
                <ul className="nav-links nav-list">
                    <li><a href="#">珍如此心</a></li>
                    <li><a href="#">视频大片</a></li>
                    <li><a href="#">珍实日常</a></li>
                    <li><a href="#">爱之花絮</a></li>
                </ul>
                <div className="arrow">
                    <img src={arrowDown} />
                </div>
            </nav>
        );
    }
};

export default PageNav;