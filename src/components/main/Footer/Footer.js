import React, { Component } from 'react';

import './c-footer.scss';
import './mobile-footer.scss';

import weibo from './assets/weibo.svg';
import instagram from './assets/instagram.svg';

class Footer extends Component {

    render() {
        return (
            <footer className="c-footer">

                <div className="black-box">
                    <div className="text-wrapper">
                        <p className="footer-heading margin-text heading">关于DPA</p>
                        <p className="margin-text paragraph">DPA（钻石生产者协会），是世界领先的钻石采矿公司国际联盟，
                            共同涵盖了世界大部分钻石生产。
                            通过“珍如此心，真如此钻”这一崭新平台，与中国消费者分享钻石的相关信息与鉴赏知识，以稀有之钻，赞美每一份至真之爱。
                        </p>
                        <p className="footer-icp icp">ICP 2882093821</p>
                    </div>
                </div>

                <div className="background-box">
                    <p className="footer-heading margin-text heading">关注DPA，开启每日珍爱</p>
                    <div className="social-images">
                        <div className="social-mini" >
                            <img src={weibo} />
                            <img src={instagram} />
                        </div>
                        <div className="wechat" >
                            <img src="#" />
                            <p className="footer-small">扫码进入官方微信账号</p>
                        </div>
                        <p className="footer-icp icp">ICP 2882093821</p>
                    </div>
                </div>
            </footer >
        );
    }
};

export default Footer;