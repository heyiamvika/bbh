import React from 'react';

import './c-header.scss';
import './mobile-header.scss';

import PageNav from '../../common/PageNav/PageNav';
import DiamondNav from '../../common/DiamondNav/DiamondNav';

class Header extends React.Component {
  render() {

    return (
      <div className="c-header">
        <PageNav />
        <div className="header-content">
          <img className="logo" src="" />
          <p className="header-text">总有一份珍贵，指引
            着每段爱走向长久
            是那颗至真的心，每天守护着爱的不变与真实
            是那颗闪耀不变的钻石，照亮着爱，直到永
          </p>
        </div>
        <DiamondNav />
      </div>
    );
  }
};

export default Header;