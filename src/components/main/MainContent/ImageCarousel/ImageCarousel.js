import React from 'react';

import './c-image-carousel.scss';
import './mobile-image-carousel.scss';

import DiamondNav from '../../../common/DiamondNav/DiamondNav';

class ImageCarousel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="c-image-carousel">
                <div className="grey-box"></div>
                <DiamondNav />
            </div >
        );
    }
};

export default ImageCarousel;