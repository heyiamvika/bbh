import React from 'react';

import './mobile-main-content.scss';

import DescriptionText from '../../../common/DescriptionText/DescriptionText';
import BlueDiv from '../../../common/BlueDiv/BlueDiv';
import VideoCarousel from '../VideoCarousel/VideoCarousel';
import WhiteDescriptionBox from '../WhiteDescriptionBox/WhiteDescriptionBox';
import ImageCarousel from '../ImageCarousel/ImageCarousel';

class MainContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="c-main-content">
                <div className="content-video">
                    <BlueDiv width="1000px" />
                    <DescriptionText />
                    <VideoCarousel />
                </div>
                <WhiteDescriptionBox />
                <DescriptionText />
                <ImageCarousel />
                <BlueDiv width="1000px" />
            </div >
        );
    }
};

export default MainContent;