import React from 'react';

import './c-white-description-box.scss';
import './mobile-white-description-box.scss';

import DescriptionText from '../../../common/DescriptionText/DescriptionText';

class WhiteDescriptionBox extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="c-white-description-box">
                <div className="picture"></div>
                <div className="grey-box"></div>
                <DescriptionText />
            </div >
        );
    }
};

export default WhiteDescriptionBox;